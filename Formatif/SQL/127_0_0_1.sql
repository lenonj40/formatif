-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2019 at 04:15 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_pizza`
--
CREATE DATABASE IF NOT EXISTS `toko_pizza` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_pizza`;

-- --------------------------------------------------------

--
-- Table structure for table `master_karyawan`
--

CREATE TABLE `master_karyawan` (
  `nik` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_karyawan`
--

INSERT INTO `master_karyawan` (`nik`, `nama`, `alamat`, `telp`, `tempat_lahir`, `tanggal_lahir`, `flag`) VALUES
('19021', 'Yudhi Selamet', 'Jl. Kebon Baru Utara No. 7', '0218883838', 'Jakarta', '1995-01-17', 1),
('19022', 'Bagus Riyadi', 'Jl. Kampung Sawah No.9', '0218838383', 'Jakarta', '1989-04-19', 1),
('19023', 'Adib Bisyri', 'Jl. Kalibaru Timur', '089621650023', 'Jakarta', '1998-12-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_menu`
--

CREATE TABLE `master_menu` (
  `kode_menu` varchar(5) NOT NULL,
  `nama_menu` varchar(30) NOT NULL,
  `harga` float NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_menu`
--

INSERT INTO `master_menu` (`kode_menu`, `nama_menu`, `harga`, `keterangan`, `flag`) VALUES
('PZ001', 'Veggie Garden Pizza', 76000, 'Jagung,Jamur,Keju Mozarella,Paprika Merah,Paprika Hitam', 1),
('PZ002', 'Tuna Meit Pizza', 80000, 'Irisan Daging Ikan Tuna,Butiran Jagung,Saus Mayonaise', 1),
('PZ003', 'Cheeseburger Pizza', 79000, 'Daging Sapi Cincang,Daging Sapi Dan Daging Ayam Amerika', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_pemesanan`
--

CREATE TABLE `transaksi_pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `nik` varchar(5) NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `nama_pelanggan` varchar(100) NOT NULL,
  `kode_menu` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `total_harga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_pemesanan`
--

INSERT INTO `transaksi_pemesanan` (`id_pemesanan`, `nik`, `tgl_pemesanan`, `nama_pelanggan`, `kode_menu`, `qty`, `total_harga`) VALUES
(1, '19021', '2019-02-16', 'Inggit Palestri', 'PZ002', 2, 160000),
(2, '19022', '2019-02-16', 'Anggun C Sasmi', 'PZ003', 1, 79000),
(3, '19023', '2019-02-23', 'Aisha', 'PZ003', 5, 395000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_karyawan`
--
ALTER TABLE `master_karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `master_menu`
--
ALTER TABLE `master_menu`
  ADD PRIMARY KEY (`kode_menu`);

--
-- Indexes for table `transaksi_pemesanan`
--
ALTER TABLE `transaksi_pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transaksi_pemesanan`
--
ALTER TABLE `transaksi_pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
