<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_model extends CI_Model{
	private $_table = "master_karyawan";

	public function TampilDataPegawai()
	{
	$query = $this->db->query("select * from master_karyawan WHERE flag = 1");
	return $query->result();
	}
	public function  save()
	{	
	$tgl = $this->input->post('tgl');
	$bln = $this->input->post('bln');
	$thn = $this->input->post('thn');
	$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;

	$data['nik']			= $this->input->post('nik');
	$data['nama']			= $this->input->post('nama_pegawai');
	$data['alamat']			= $this->input->post('alamat');
	$data['telp']			= $this->input->post('telp');
	$data['tempat_lahir']	= $this->input->post('tempat_lahir');
	$data['tanggal_lahir']	= $tgl_gabung;
	$data['flag']			= 1;
	$this->db->insert($this->_table, $data);
	}
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function edit($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function update($nik)
	{
	$tgl = $this->input->post('tgl');
	$bln = $this->input->post('bln');
	$thn = $this->input->post('thn');
	$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;

	$data['nik']			= $this->input->post('nik');
	$data['nama']			= $this->input->post('nama_pegawai');
	$data['alamat']			= $this->input->post('alamat');
	$data['telp']			= $this->input->post('telp');
	$data['tempat_lahir']	= $this->input->post('tempat_lahir');
	$data['tanggal_lahir']	= $tgl_gabung;
	$data['flag']			= 1;
	$this->db->where('nik', $nik);
	$this->db->update($this->_table, $data);
	}
	public function delete($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
	}
}