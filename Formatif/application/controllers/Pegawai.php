<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Pegawai extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("pegawai_model");
	$this->load->model("menu_model");
}
	public function index()
{
	$this->ListPegawai();

}
	public function ListPegawai()
{
	$data['data_pegawai']= $this->pegawai_model->tampilDataPegawai();
	$this->load->view('Menu',$data);
}
public function InputPegawai()
{
	$data['data_pegawai']= $this->pegawai_model->tampilDataPegawai();
	if (!empty($_REQUEST)) {
		$m_pegawai = $this->pegawai_model;
		$m_pegawai->save();
		redirect("pegawai/index", "refresh");
	}
	$this->load->view('input',$data);
 }
 public function EditPegawai($nik)
{
	$data['detail_pegawai'] = $this->pegawai_model->detail($nik);
	if (!empty($_REQUEST)) {
		$m_pegawai = $this->pegawai_model;
		$m_pegawai->update($nik);
		redirect("pegawai/index", "refresh");
}
	$this->load->view('edit',$data);
 	}
 	public function deletepegawai($nik)
 	{
 		$m_pegawai = $this->pegawai_model;
 		$m_pegawai->delete($nik);
 		redirect("pegawai/index", "refresh");
 	}
}