<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class menu extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("Menu_model");
}

	public function index()
{
	$this->listmenu();

}

	public function listmenu()
{
	$data['data_menu']= $this->Menu_model->tampilDataMenu();
	$this->load->view('Menu1',$data);
}
public function InputMenu()
{
	if (!empty($_REQUEST)) {
		$m_menu = $this->Menu_model;
		$m_menu->save();
		redirect("menu/index", "refresh");
	}
	$this->load->view('input_menu1');
 }
 public function EditMenu($kode_menu)
{
	$data['detail_menu'] = $this->Menu_model->detail($kode_menu);
	if (!empty($_REQUEST)) {
		$m_menu = $this->Menu_model;
		$m_menu->update($kode_menu);
		redirect("menu/index", "refresh");
}
	$this->load->view('edit1',$data);
 	}
 	public function deletemenu($kode_menu)
 	{
 		$m_menu = $this->menu_model;
 		$m_menu->delete($kode_menu);
 		redirect("menu/index", "refresh");
 	}
}