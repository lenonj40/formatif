 <html>
<head>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>UTS 2018</title>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style.css">
</head>

<body bgcolor="#999999">
<div class="utama">
	<div class="header">
    	<div class="h-col1">
        	<img src="images/logo.png" width="125px" />
        </div>
        <div class="h-col2">
        	<span>Toko Pizza</span>
        </div>
        <div class="pic-admin">
            <div class="nm-admin">Administrator</div> <img src="images/admin.png" width="75px" />
        </div>
    </div>
    <div class="menu">
    	<div class="menu_1">
        	<span><a href="<?=base_url();?>main">Home</a></span>
        </div>
    	<div class="menu_2">
        	<a href="<?=base_url();?>pegawai/listpegawai"><span>Data Pegawai</span></a>
        </div>
    	<div class="menu_3">
        	<a href="<?=base_url();?>menu/listmenu"><span>Menu Pizza</span></a>
        </div>
        <div class="menu_4">
        	<a href="<?=base_url();?>transaksi/listtransaksi"><span>Pemesanan</span></a>
        </div>
    </div>
    <div class="konten">
    	<!----------------------------------------- ISI KONTEN/TABEL DSINI ---------------------------------------------->
 <table width="1350" height="31" border="0">
  <tr> <td><a href="<?=base_url();?>Pegawai/InputPegawai">Tambah Data</a></td>
    <td width="1295"><div align="center">&quot;Data Pegawai&quot;</div></td>
  </tr>
   <tr>
  <td colspan="6" bgcolor="#6666FF" align="right"><label for="Cari Nama"></label>
  <input type="text" name="Cari Nama" id="Cari Nama" placeholder="Cari Nama" />
  <input name="Cari Nama" type="button" value="Cari Data" /> 
  </td>
  </tr>
</table>
<?php
      date_default_timezone_set('Asia/Jakarta');
    echo date('l, d F Y h:i:s A')
    ?>
<table width="1350" height="107" border="1">
  <tr bgcolor="#CCFF99">
    <td width="77" height="30">No</td>
    <td width="87">Nik</td>
    <td width="133">Nama Pegawai</td>
    <td width="192">Alamat</td>
    <td width="192">Telp</td>
    <td width="192">Tempat Lahir</td>
    <td width="192">Tanggal Lahir</td>
    <td width="295">Aksi</td>
  </tr>
  <?php
  $no = 0 ;
  foreach ($data_pegawai as $data){
  $no++;
  ?>
  <tr bgcolor="#CCCCCC">
    <td height="24"><?= $no;?></td>
    <td><?= $data->nik; ?></td>
    <td><?= $data->nama; ?></td>
    <td><?= $data->alamat; ?></td>
     <td><?= $data->telp; ?></td>
      <td><?= $data->tempat_lahir; ?></td>
       <td><?= $data->tanggal_lahir; ?></td>
   <td><a href="<?=base_url();?>pegawai/editpegawai/<?= $data->nik; ?>"/> Edit  </a>
      | <a href="<?=base_url();?>pegawai/deletepegawai/<?= $data->nik; ?>" onClick="return confirm('Yakin Ingin Hapus Data?');"/>Delete</a></td> 
  </tr>
  <?php } ?>
</table>
        
        <!--------------------------------------------------------------------------------------------------------------->
    </div>
</div>
</body>
</html>