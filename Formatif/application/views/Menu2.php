  <html>
<head>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Toko Pizza</title>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/logo.png">
</head>

<body bgcolor="#999999">
<div class="utama">
	<div class="header">
    	<div class="h-col1">
        	<img src="images/logo.png" width="125px" />
        </div>
        <div class="h-col2">
        	<span>Toko Pizza</span>
        </div>
        <div class="pic-admin">
            <div class="nm-admin">Administrator</div> <img src="images/admin.png" width="75px" />
        </div>
    </div>
    <div class="menu">
    	<div class="menu_1">
        	<span><a href="<?=base_url();?>main">Home</a></span>
        </div>
    	<div class="menu_2">
        	<a href="<?=base_url();?>Pegawai/ListPegawai"><span>Data Pegawai</span></a>
        </div>
    	<div class="menu_3">
        	<a href="<?=base_url();?>menu/listmenu"><span>Menu Pizza</span></a>
        </div>
        <div class="menu_4">
        	<a href="<?=base_url();?>transaksi/listtransaksi"><span>Pemesanan</span></a>
        </div>
    </div>
    <div class="konten">
    	<!----------------------------------------- ISI KONTEN/TABEL DSINI ---------------------------------------------->
   <table width="1134" height="31" border="0">
  <tr>  <td width="61"><a href="<?=base_url();?>Transaksi/InputPemesanan">Tambah Data</a></td>
    <td width="1063"><div align="center">&quot;Data Pemesanan&quot;</div></td>
  </tr>
   <tr>
  <td colspan="6" bgcolor="#6666FF" align="right"><label for="Cari Nama"></label>
  <input type="text" name="Cari Nama" id="Cari Nama" placeholder="Cari Nama" />
  <input name="Cari Nama" type="button" value="Cari Data" /> 
  </td>
  </tr>
</table>
<?php
      date_default_timezone_set('Asia/Jakarta');
    echo date('l, d F Y h:i:s A')
    ?>
<table width="1171" height="107" border="1">
  <tr bgcolor="#CCFF99">
    <td width="84" height="30">No</td>
    <td width="145">Nama Pegawai</td>
    <td width="145">Tanggal Pesan</td>
    <td width="240">Nama Pelanggan</td>
    <td width="161">Nama Menu</td>
    <td width="149">Qty</td>
    <td width="201">Total Harga</td>
  </tr>
  <?php
  $no = 0 ;
  foreach ($data_transaksi as $data){
  $no++;
  ?>
  <tr bgcolor="#CCCCCC">
    <td height="24"><?= $no;?></td>
    <td><?= $data->nama; ?></td>
    <td><?= $data->tgl_pemesanan; ?></td>
     <td><?= $data->nama_pelanggan; ?></td>
      <td><?= $data->nama_menu; ?></td>
       <td><?= $data->qty; ?></td>
        <td  align="right">Rp. <?= number_format($data->total_harga); ?> ,-</td>
  </tr>
  <?php
   } ?>
</table>   <!--------------------------------------------------------------------------------------------------------------->
    </div>
</div>
</body>
</html>