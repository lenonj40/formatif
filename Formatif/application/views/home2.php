<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Toko Pizza</title>
</head>
<body>
<table>
<p align="center">&nbsp;	</p>
<div style="background:#333;color:#fff;margin:5px 0;padding:10px 0"><marquee direction="left" scrolldelay="20" truespeed="truespeed" scrollamount="1" onmouseover="this.stop()" onmouseout="this.start()">Selamat Datang Di Web Toko Kami</marquee></div>
<table width="1350" height="57" border="1">
  <tr height="35">
   <div align="left"><a href="InputPemesanan">[Input Pesanan]</a></div>
    <td bgcolor="#CC0A16"colspan="7"><div style=color:#fff; align="center">Toko Pizza</div></td>
  </tr>
  <tr height="35" align="center" bgcolor="#FFFF00">
   <td bgcolor="#66FFFF" width="334"><a href="<?=base_url();?>main">Home</td>
    <td bgcolor="#66FFFF" width="334"><a href="<?=base_url();?>pegawai/listpegawai"> Data Pegawai</td>
    <td bgcolor="#66FFFF" width="334"><a href="<?=base_url();?>menu/listmenu"> Menu Pizza</td>
    <td bgcolor="#66FFFF" width="334"><a href="<?=base_url();?>transaksi/listtransaksi"> Transaksi Pemesanan</td>
  </tr>
</table>
<table width="1350" height="31" border="0">
  <tr>
    <td width="1295"><div align="center">&quot;Data Pemesanan&quot;</div></td>
  </tr>
   <tr>
  <td colspan="6" bgcolor="#6666FF" align="right"><label for="Cari Nama"></label>
  <input type="text" name="Cari Nama" id="Cari Nama" placeholder="Cari Nama" />
  <input name="Cari Nama" type="button" value="Cari Data" /> 
  </td>
  </tr>
</table>
<?php
      date_default_timezone_set('Asia/Jakarta');
    echo date('l, d F Y h:i:s A')
    ?>
<table width="1350" height="107" border="1">
  <tr bgcolor="#CCFF99">
    <td width="77" height="30">No</td>
    <td width="133">Nama Pegawai</td>
    <td width="192">Tanggal Pesan</td>
    <td width="192">Nama Pelanggan</td>
    <td width="192">Nama Menu</td>
    <td width="192">Qty</td>
    <td width="192">Total Harga</td>
  </tr>
  <?php
  $no = 0 ;
  foreach ($data_transaksi as $data){
  $no++;
  ?>
  <tr bgcolor="#CCCCCC">
    <td height="24"><?= $no;?></td>
    <td><?= $data->nama; ?></td>
    <td><?= $data->tgl_pemesanan; ?></td>
     <td><?= $data->nama_pelanggan; ?></td>
      <td><?= $data->nama_menu; ?></td>
       <td><?= $data->qty; ?></td>
        <td  align="right">Rp. <?= number_format($data->total_harga); ?> ,-</td>
  </tr>
  <?php
   } ?>
</table>
<div style="background:#333;color:#fff;margin:5px 0;padding:10px 0" align="center" class='copyright'>Copyright © 2019. Adib Bisyri Musyaffa </div>
<p align="center">&nbsp;</p>
</body>
</html>